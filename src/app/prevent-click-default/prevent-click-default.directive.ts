import {Directive, HostListener} from '@angular/core';

@Directive({
  selector: '[appPreventClickDefault]'
})
export class PreventClickDefaultDirective {

  @HostListener('click', ['$event']) preventClickDefault(event: MouseEvent) {
    console.log(event);
    event.preventDefault();
  }

  constructor() { }

}
