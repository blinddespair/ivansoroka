import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PreventClickDefaultDirective} from './prevent-click-default.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [PreventClickDefaultDirective],
  exports: [PreventClickDefaultDirective]
})
export class PreventClickDefaultModule { }
