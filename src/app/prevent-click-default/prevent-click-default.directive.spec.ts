import { PreventClickDefaultDirective } from './prevent-click-default.directive';

describe('PreventClickDefaultDirective', () => {
  it('should call event preventDefault', () => {
    const directive = new PreventClickDefaultDirective();
    const event = new MouseEvent('');
    spyOn(event, 'preventDefault');
    directive.preventClickDefault(event);
    expect(event.preventDefault).toHaveBeenCalled();
  });
});
