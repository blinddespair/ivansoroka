import {ActionReducerMap} from '@ngrx/store';
import {imageViewerReducer, ImageViewerState} from './image-viewer/image-viewer.reducer';


export interface AppState {
  imageViewer: ImageViewerState;
}

export interface BooksStopAppAction {
  type: string;
  payload?: any;
}

export const reducers: ActionReducerMap<AppState> = {
  imageViewer: imageViewerReducer
};
