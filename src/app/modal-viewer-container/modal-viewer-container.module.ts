import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalViewerContainerComponent } from './modal-viewer-container.component';
import {ImageInfoComponent} from '../image-info/image-info.component';
import {ImageInfoModule} from '../image-info/image-info.module';
import {StopClickPropagationModule} from '../stop-click-propagation/stop-click-propagation.module';

@NgModule({
  imports: [
    CommonModule,
    StopClickPropagationModule,
    ImageInfoModule
  ],
  declarations: [ModalViewerContainerComponent],
  exports: [ModalViewerContainerComponent],
  entryComponents: [ImageInfoComponent]
})
export class ModalViewerContainerModule { }
