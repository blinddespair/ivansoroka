import {
  Component, ComponentFactoryResolver, ComponentRef, HostBinding, OnDestroy, OnInit, ReflectiveInjector, Type, ViewChild,
  ViewContainerRef
} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {ModalViewerService} from './shared/modal-viewer.service';
import 'rxjs/add/operator/takeUntil';

@Component({
  selector: 'app-modal-viewer-container',
  templateUrl: './modal-viewer-container.component.html',
  styleUrls: ['./modal-viewer-container.component.scss']
})
export class ModalViewerContainerComponent implements OnInit, OnDestroy {

  readonly VISIBILITY_OPTIONS = {
    VISIBLE: 'visible',
    HIDDEN: 'hidden'
  };

  @ViewChild('content', {read: ViewContainerRef}) content: ViewContainerRef;
  @HostBinding('style.visibility') visibility: string = this.VISIBILITY_OPTIONS.HIDDEN;

  private componentDestroyed = new Subject();

  constructor(
    private resolver: ComponentFactoryResolver,
    private modalPopupWindowService: ModalViewerService
  ) {
  }

  ngOnInit() {
    this.modalPopupWindowService.displayModalWindow
      .takeUntil(this.componentDestroyed)
      .subscribe(event => this.displayWindow(event));
    this.modalPopupWindowService.closeModalWindow
      .takeUntil(this.componentDestroyed)
      .subscribe(event => {
        this.onClosingModalPopupWindow();
      });
  }

  ngOnDestroy() {
    this.componentDestroyed.next(true);
    this.componentDestroyed.complete();
  }

  displayWindow(event) {
    const component =  this.createPopupableFromComponentType(event.modalContentComponent, event.data);
    this.displayComponentDetails(event, component);
  }

  private createPopupableFromComponentType<T>(type: Type<T>, data): ComponentRef<T> {
    const injector = ReflectiveInjector.resolveAndCreate([
      {provide: 'data', useValue: data}
    ]);
    const factory = this.resolver.resolveComponentFactory(type);
    return factory.create(injector);
  }

  displayComponentDetails(event: any, component: any)	{
    this.content.insert(component.hostView);
    this.visibility = this.VISIBILITY_OPTIONS.VISIBLE;
  }

  closeModalPopupWindow() {
    this.modalPopupWindowService.closeModalWindow.emit();
  }

  onClosingModalPopupWindow() {
    this.content.clear();
    this.visibility = this.VISIBILITY_OPTIONS.HIDDEN;
  }

}
