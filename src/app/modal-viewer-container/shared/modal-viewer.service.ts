import {EventEmitter, Injectable} from '@angular/core';

@Injectable()
export class ModalViewerService {

  displayModalWindow: EventEmitter<any> = new EventEmitter();
  closeModalWindow: EventEmitter<any> = new EventEmitter();

  constructor() { }

}
