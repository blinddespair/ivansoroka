import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {Store} from '@ngrx/store';
import {AppState} from '../app.reducers';
import {ApiService} from '../api/api.service';
import {ImageViewerAction} from './image-viewer.reducer';

@Injectable()
export class ImageViewerEffects {
  @Effect()
  searchImages = this.apiService.serviceCall(
    this.store,
    this.actions,
    ImageViewerAction.SEARCH_IMAGES,
    payload => {
      return this.apiService.searchImages(payload.searchParams);
    },
    response => {
      return {
        type: ImageViewerAction.SAVE_SEARCH_RESULT,
        payload: {
          images: response
        }
      };
    },
    () => {
      console.log('request failed');
    }
  );

  constructor(
    private actions: Actions,
    private store: Store<AppState>,
    private apiService: ApiService
  ) {}

}
