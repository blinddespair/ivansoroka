import {Component, EventEmitter, Output} from '@angular/core';
import {ImageSearchParams} from './shared/image-search-params.model';
import * as _ from 'lodash';

@Component({
  selector: 'app-image-search',
  templateUrl: './image-search.component.html',
  styleUrls: ['./image-search.component.scss']
})
export class ImageSearchComponent {
  @Output() searchImages: EventEmitter<ImageSearchParams> = new EventEmitter();
  selectedTags: string[] = [];

  constructor() { }

  onSearch(searchParams) {
    const searchTags = this.getTagsFromSearchParams(searchParams);
    const tags = _.concat(searchTags, this.selectedTags);
    if (tags.length > 0) {
      this.searchImages.emit({
        tags: tags
      });
    }
  }

  getTagsFromSearchParams(searchParams) {
    if (_.isEmpty(searchParams.tags)) {
      return [];
    }
    return searchParams.tags
      .split(',')
      .map(tag => tag.trim());
  }

  toggleTag(event: { isEnabled: boolean, tag: string}) {
    if (event.isEnabled) {
      this.selectedTags.push(event.tag);
    } else {
      this.selectedTags = this.selectedTags.filter(tag => tag !== event.tag);
    }
  }

}
