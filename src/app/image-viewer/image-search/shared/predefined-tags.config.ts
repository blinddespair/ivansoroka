import {PredefinedTags} from './predefined-tags.model';
import * as _ from 'lodash';

export const preDefinedTags: PredefinedTags = {
  categories: [
    {
      name: 'Vehicle',
      sabCategories: [
        {
          name: 'Car',
          tags: ['BMW', 'Audi', 'Ford', 'Fiat']
        },
        {
          name: 'Bus',
          tags: ['Icarus', 'Volvo']
        }
      ]
    },
    {
      name: 'Location',
      sabCategories: [
        {
          name: 'City',
          tags: ['Budapest', 'Boston', 'London', 'Kyiv']
        }
      ]
    }
  ]
};

function predefinedTagsFactory() {
  return _.cloneDeep(preDefinedTags);
}

export const predefinedTagsProvider = {
  provide: preDefinedTags, useFactory: predefinedTagsFactory
};
