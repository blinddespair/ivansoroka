export interface PredefinedTags {
  categories: {
    name: string;
    isCollapsed?: boolean;
    sabCategories: {
      name: string;
      isCollapsed?: boolean;
      tags: string[];
    }[];
  }[];
}
