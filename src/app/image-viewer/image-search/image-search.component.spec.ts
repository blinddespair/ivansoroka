import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageSearchComponent } from './image-search.component';
import {FormsModule} from '@angular/forms';
import {PreventClickDefaultModule} from '../../prevent-click-default/prevent-click-default.module';
import {BsDropdownModule, CollapseModule} from 'ngx-bootstrap';
import {CommonModule} from '@angular/common';
import {PredefinedTagsSelectionComponent} from './predefined-tags-selection/predefined-tags-selection.component';

describe('ImageSearchComponent', () => {
  let component: ImageSearchComponent;
  let fixture: ComponentFixture<ImageSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        BsDropdownModule.forRoot(),
        CollapseModule.forRoot(),
        FormsModule,
        PreventClickDefaultModule
      ],
      declarations: [
        ImageSearchComponent,
        PredefinedTagsSelectionComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('getTagsFromSearchParams', () => {
    it('should split comma separated string into tags array', () => {
      const initialString = 'white chocolate,Budapest,Audi';
      const expectedTags = ['white chocolate', 'Budapest', 'Audi'];

      const tags = component.getTagsFromSearchParams({ tags: initialString });

      expect(tags).toEqual(expectedTags);
    });

    it('should split comma separated string into tags array if also contains spaces', () => {
      const initialString = 'white chocolate, Budapest,Audi, BMW';
      const expectedTags = ['white chocolate', 'Budapest', 'Audi', 'BMW'];

      const tags = component.getTagsFromSearchParams({tags: initialString});

      expect(tags).toEqual(expectedTags);
    });

    it('should return empty array for empty string', () => {
      const initialString = '';
      const expectedTags = [];

      const tags = component.getTagsFromSearchParams({tags: initialString});

      expect(tags).toEqual(expectedTags);
    });
  });

  describe('onSearch', () => {
    it('should emit searchImages', () => {
      const tags = 'water, stones';

      spyOn(component.searchImages, 'emit');
      component.onSearch({ tags: tags });

      expect(component.searchImages.emit).toHaveBeenCalled();
    });

    it('should not emit searchImages if there are no tags', () => {
      const tags = '';

      spyOn(component.searchImages, 'emit');
      component.onSearch({ tags: tags });

      expect(component.searchImages.emit).not.toHaveBeenCalled();
    });

    it('should emit searchImages when searched tags are empty but there are selected predefined tags', () => {
      const tags = '';
      component.selectedTags = ['Budapest', 'Kyiv'];

      spyOn(component.searchImages, 'emit');
      component.onSearch({ tags: tags });

      expect(component.searchImages.emit).toHaveBeenCalled();
    });
  });

  describe('toggleTag', () => {
    it('should add selected predefined tag', () => {
      const event = { isEnabled: true, tag: 'Budapest' };
      const expectedSelectedTags = ['Budapest'];

      component.toggleTag(event);

      expect(component.selectedTags).toEqual(expectedSelectedTags);
    });

    it('should remove unselected predefined tag', () => {
      const event = { isEnabled: false, tag: 'Budapest' };
      component.selectedTags = ['Budapest', 'water'];
      const expectedSelectedTags = ['water'];

      component.toggleTag(event);

      expect(component.selectedTags).toEqual(expectedSelectedTags);
    });
  });
});
