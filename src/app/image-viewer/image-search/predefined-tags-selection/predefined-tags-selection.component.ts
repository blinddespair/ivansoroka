import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {PredefinedTags} from '../shared/predefined-tags.model';
import {preDefinedTags, predefinedTagsProvider} from '../shared/predefined-tags.config';

@Component({
  selector: 'app-predefined-tags-selection',
  templateUrl: './predefined-tags-selection.component.html',
  styleUrls: ['./predefined-tags-selection.component.scss'],
  providers: [
    predefinedTagsProvider
  ]
})
export class PredefinedTagsSelectionComponent implements OnInit {
  @Output() toggleTag: EventEmitter<{isEnabled: boolean, tag: string}> = new EventEmitter();

  constructor(@Inject(preDefinedTags) public predefinedTags: PredefinedTags) {}

  ngOnInit() {
  }

  toggleCategory(category) {
    category.isCollapsed = !category.isCollapsed;
  }

  onToggleTag(event, tag) {
    this.toggleTag.emit({
      isEnabled: event.target.checked,
      tag: tag
    });
  }

}
