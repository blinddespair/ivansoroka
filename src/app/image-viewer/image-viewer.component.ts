import { Component } from '@angular/core';
import {AppState} from '../app.reducers';
import {Store} from '@ngrx/store';
import {getFoundImagesState, ImageViewerAction} from './image-viewer.reducer';
import {Observable} from 'rxjs/Observable';
import {Image} from './shared/image.model';
import {ImageSearchParams} from './image-search/shared/image-search-params.model';

@Component({
  selector: 'app-image-viewer',
  templateUrl: './image-viewer.component.html',
  styleUrls: ['./image-viewer.component.scss']
})
export class ImageViewerComponent {
  imagesState: Observable<Image[]>;

  constructor(private store: Store<AppState>) {
    this.imagesState = this.store.select(getFoundImagesState);
  }

  searchImages(searchParams: ImageSearchParams) {
    this.store.dispatch({
      type: ImageViewerAction.SEARCH_IMAGES,
      payload: {
        searchParams: searchParams
      }
    });
  }

}
