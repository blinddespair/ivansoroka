import {Action, createSelector} from '@ngrx/store';
import {AppState} from '../app.reducers';
import {Image} from './shared/image.model';

export interface ImageViewerState {
  searchParams?: any; // we store search params, so when paging is implemented we can simply access searchParams from here to
  // load images for other page
  images: Image[];
}

export class ImageViewerAction implements Action {
  static SEARCH_IMAGES = 'SEARCH_IMAGES';
  static SAVE_SEARCH_RESULT = 'SAVE_SEARCH_RESULT';

  type: string;
  payload?: any;
}

const INITIAL_IMAGE_VIEWER_STATE: ImageViewerState = {
  images: []
};

export function imageViewerReducer(state: ImageViewerState = INITIAL_IMAGE_VIEWER_STATE, action: ImageViewerAction): ImageViewerState {
  const type = action.type;
  const payload = action.payload;

  switch (type) {
    case ImageViewerAction.SEARCH_IMAGES:
      return Object.assign({}, state, {
        searchParams: payload.searchParams
      });

    case ImageViewerAction.SAVE_SEARCH_RESULT:
      return Object.assign({}, state, {
        images: payload.images
      });

    default:
      return state;
  }
}

export const getImageViewerState = (appState: AppState) => appState.imageViewer;
export const getFoundImagesState = createSelector(getImageViewerState, imageViewerState => {
  return imageViewerState.images;
});
