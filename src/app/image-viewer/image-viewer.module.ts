import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ImageViewerComponent} from './image-viewer.component';
import {EffectsModule} from '@ngrx/effects';
import {ImageViewerEffects} from './image-viewer.effects';
import {ImageListModule} from '../image-list/image-list.module';
import {ModalViewerContainerModule} from '../modal-viewer-container/modal-viewer-container.module';
import {ModalViewerService} from '../modal-viewer-container/shared/modal-viewer.service';
import {ImageSearchComponent} from './image-search/image-search.component';
import {FormsModule} from '@angular/forms';
import {PredefinedTagsSelectionComponent} from './image-search/predefined-tags-selection/predefined-tags-selection.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import {CollapseModule} from 'ngx-bootstrap';
import {PreventClickDefaultModule} from '../prevent-click-default/prevent-click-default.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    EffectsModule.forFeature([ImageViewerEffects]),
    BsDropdownModule.forRoot(),
    CollapseModule.forRoot(),
    ImageListModule,
    ModalViewerContainerModule,
    PreventClickDefaultModule
  ],
  declarations: [
    ImageViewerComponent,
    ImageSearchComponent,
    PredefinedTagsSelectionComponent
  ],
  exports: [
    ImageViewerComponent
  ],
  providers: [
    ModalViewerService
  ]
})
export class ImageViewerModule { }
