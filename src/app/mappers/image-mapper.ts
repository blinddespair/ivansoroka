import { Injectable } from '@angular/core';
import {PhotoEntity} from '../image-viewer/shared/image-search-response.model';
import {Image} from '../image-viewer/shared/image.model';

@Injectable()
export class ImageMapper {
  mapPhotoEntityToImage(photoEntity: PhotoEntity): Image {
    return {
      id: photoEntity.id,
      title: photoEntity.title,
      url: this.combineImageUrlFromPhotoEntity(photoEntity)
    };
  }

  private combineImageUrlFromPhotoEntity(photoEntity: PhotoEntity): string {
    return `https://farm${photoEntity.farm}.staticflickr.com/${photoEntity.server}/${photoEntity.id}_${photoEntity.secret}.jpg`;
  }
}
