export interface ImageDetails {
  id: string;
  secret: string;
  server: string;
  farm: number;
  dateuploaded: string;
  isfavorite: number;
  license: number;
  safety_level: number;
  rotation: number;
  originalsecret: string;
  originalformat: string;
  owner: Owner;
  title: TitleOrDescription;
  description: TitleOrDescription;
  visibility: Visibility;
  dates: Dates;
  views: string;
  editability: EditabilityOrPubliceditability;
  publiceditability: EditabilityOrPubliceditability;
  usage: Usage;
  comments: Comments;
  notes: Notes;
  people: People;
  tags: Tags;
  urls: Urls;
  media: string;
}
export interface Owner {
  nsid: string;
  username: string;
  realname: string;
  location: string;
  iconserver: string;
  iconfarm: number;
  path_alias: string;
}
export interface TitleOrDescription {
  _content: string;
}
export interface Visibility {
  ispublic: number;
  isfriend: number;
  isfamily: number;
}
export interface Dates {
  posted: string;
  taken: string;
  takengranularity: number;
  takenunknown: number;
  lastupdate: string;
}
export interface EditabilityOrPubliceditability {
  cancomment: number;
  canaddmeta: number;
}
export interface Usage {
  candownload: number;
  canblog: number;
  canprint: number;
  canshare: number;
}
export interface Comments {
  _content: number;
}
export interface Notes {
  note?: (null)[] | null;
}
export interface People {
  haspeople: number;
}
export interface Tags {
  tag?: (TagEntity)[] | null;
}
export interface TagEntity {
  id: string;
  author: string;
  authorname: string;
  raw: string;
  _content: string;
  machine_tag: number;
}
export interface Urls {
  url?: (UrlEntity)[] | null;
}
export interface UrlEntity {
  type: string;
  _content: string;
}
