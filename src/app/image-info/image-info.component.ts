import {Component, Injector, OnInit} from '@angular/core';
import {Image} from '../image-viewer/shared/image.model';
import * as _ from 'lodash';
import {ApiService} from '../api/api.service';
import 'rxjs/add/operator/delay';
import {ImageDetails} from './shared/image-details.model';

@Component({
  selector: 'app-image-info',
  templateUrl: './image-info.component.html',
  styleUrls: ['./image-info.component.scss']
})
export class ImageInfoComponent implements OnInit {
  image: Image;
  imageDetails: ImageDetails;
  isLoading: boolean;

  constructor(private injector: Injector, private apiService: ApiService) {
    this.image = _.get(injector.get('data'), 'image', null);
  }

  ngOnInit() {
    this.fetchImageDetails();
  }

  fetchImageDetails() {
    this.isLoading = true;
    this.apiService.fetchImageDetails(this.image.id)
      .subscribe(
        imageDetails => this.imageDetails = imageDetails,
        () => {},
        () => {
          this.isLoading = false;
        });
  }

}
