import {Component, Input, OnInit} from '@angular/core';
import {Image} from '../../image-viewer/shared/image.model';
import {ModalViewerService} from '../../modal-viewer-container/shared/modal-viewer.service';
import {ImageInfoComponent} from '../../image-info/image-info.component';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit {
  @Input() image: Image;

  constructor(private modalViewerService: ModalViewerService) { }

  ngOnInit() {
  }

  openImageDetails() {
    this.modalViewerService.displayModalWindow.emit({
      modalContentComponent: ImageInfoComponent,
      data: {
        image: this.image
      }
    });
  }

}
