import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageListComponent } from './image-list.component';
import {ImageComponent} from './image/image.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ImageListComponent,
    ImageComponent
  ],
  exports: [
    ImageListComponent
  ]
})
export class ImageListModule { }
