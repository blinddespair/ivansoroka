import {Component, Input, OnInit} from '@angular/core';
import {Image} from '../image-viewer/shared/image.model';

@Component({
  selector: 'app-image-list',
  templateUrl: './image-list.component.html',
  styleUrls: ['./image-list.component.scss']
})
export class ImageListComponent implements OnInit {
  @Input() images: Image[];

  constructor() { }

  ngOnInit() {
  }

}
