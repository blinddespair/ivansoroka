import { Injectable } from '@angular/core';
import {AppState, BooksStopAppAction} from '../app.reducers';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/empty';
import * as _ from 'lodash';
import {Actions} from '@ngrx/effects';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/withLatestFrom';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/finally';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {ImageMapper} from '../mappers/image-mapper';
import {PhotoEntity} from '../image-viewer/shared/image-search-response.model';
import {ImageDetails} from '../image-info/shared/image-details.model';
import {ImageSearchParams} from '../image-viewer/image-search/shared/image-search-params.model';

// This service could be implementation of interface service, which could have other implementations for example for mocks
@Injectable()
export class ApiService {
  flickrApiBaseUrl = environment.flickrApiBaseUrl;

  constructor(private http: HttpClient, private imageMapper: ImageMapper) { }

  searchImages(searchParams: ImageSearchParams) {
    // generally paging could be also implemented but as part of this task I will not do it as it was not explicitly required
    const queryParameters: Array<[string, string]> = [
      ['method', 'flickr.photos.search'],
      ['api_key', environment.flickrApiKey],
      ['tags', searchParams.tags.join(',')],
      ['format', 'json'],
      ['nojsoncallback', '1']
    ];
    const url = this.flickrApiBaseUrl + this.encodedParameters(queryParameters);
    return this.http.get(url)
      .map((res: any) => res.photos.photo.map((photoEntity: PhotoEntity) => this.imageMapper.mapPhotoEntityToImage(photoEntity)))
      .catch(err => Observable.throw(err));
  }

  fetchImageDetails(imageId: string): Observable<ImageDetails> {
    const queryParameters: Array<[string, string]> = [
      ['method', 'flickr.photos.getInfo'],
      ['api_key', environment.flickrApiKey],
      ['photo_id', imageId],
      ['format', 'json'],
      ['nojsoncallback', '1']
    ];
    const url = this.flickrApiBaseUrl + this.encodedParameters(queryParameters);
    return this.http.get(url)
      .map((res: any) => res.photo)
      .catch(err => Observable.empty());
  }

  encodedParameters(params: Array<[string, string]>) {
    const encode = ([key, value]) => `${encodeURI(key)}=${encodeURI(value)}`;
    return _.reduce(_.tail(params), (acc, param) => acc + '&' + encode(param), `?${encode(_.head(params))}`);
  }

  /**
   * Generic effect template for service calls. For every action of a given type, service calls are triggered with its
   * payload (pending calls are cancelled). The success/error result of the service invocation is then passed to the
   * onSuccess/onError action creators, respectively.
   * @param {Store<AppState>} store
   * @param {Actions} actions
   * @param {string[] | string} triggeredBy
   * @param methodWithServiceCall
   * @param onSuccess
   * @param {Function} onError
   * @param {(...args: any[]) => void} onComplete
   * @returns {Observable<any>}
   */
  serviceCall(
    store: Store<AppState>,
    actions: Actions,
    triggeredBy: string[] | string,
    methodWithServiceCall: any,
    onSuccess: any,
    onError: Function,
    onComplete = _.noop
  ): Observable<any> {
    // Note: to support the way we used it so far, it's allowed to pass in string
    const triggerActions = _.isString(triggeredBy) ? [triggeredBy] : triggeredBy;
    const safeServiceCall = (payload, latestStore) => {
      try {
        return methodWithServiceCall(payload, latestStore);
      } catch (err) {
        return Observable.throw(err);
      }
    };
    return actions.ofType(...triggerActions)
      .map((action: BooksStopAppAction) => action.payload)
      .withLatestFrom(store)
      .switchMap(([payload, latestStore]) => {
          return safeServiceCall(payload, latestStore)
            .map(result => onSuccess(result, payload, latestStore))
            .catch(error => {
              onError(error);
              return Observable.empty();
            })
            .finally(() => onComplete());
        }
      );
  }
}
