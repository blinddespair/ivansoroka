import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {StopClickPropagationDirective} from './stop-click-propagation.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    StopClickPropagationDirective
  ],
  exports: [
    StopClickPropagationDirective
  ]
})
export class StopClickPropagationModule { }
