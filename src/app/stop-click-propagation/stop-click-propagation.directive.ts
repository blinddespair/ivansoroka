import {Directive, HostListener} from '@angular/core';

@Directive({
  selector: '[appStopClickPropagation]'
})
export class StopClickPropagationDirective {

  @HostListener('click', ['$event']) stopClickPropagation(event: MouseEvent) {
    event.stopPropagation();
  }

  constructor() { }

}
