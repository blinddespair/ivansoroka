import { StopClickPropagationDirective } from './stop-click-propagation.directive';

describe('StopClickPropagationDirective', () => {
  it('should call event stop propagation', () => {
    const directive = new StopClickPropagationDirective();
    const event = new MouseEvent('');
    spyOn(event, 'stopPropagation');
    directive.stopClickPropagation(event);
    expect(event.stopPropagation).toHaveBeenCalled();
  });
});
