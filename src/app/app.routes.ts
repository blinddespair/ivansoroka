import {Routes} from '@angular/router';
import {ImageViewerComponent} from './image-viewer/image-viewer.component';

export const ROUTES: Routes = [
  { path: '', component: ImageViewerComponent, pathMatch: 'full' }
];
