import {$, $$, browser} from 'protractor';

export class AppPage {
  public searchImagesTagInput = $('app-image-search input[name="tags"]');
  public searchImagesSubmitButton = $('app-image-search button[type="submit"]');
  public imageComponents = $$('app-image');
  public imageDetailsComponent = $('app-modal-viewer-container app-image-info');
  public imageDetailsLoadingWheel = $('app-modal-viewer-container app-image-info div.loading');
  public imageDetailsInfoContainer = $('app-modal-viewer-container app-image-info div.image-info-container');

  navigateTo() {
    return browser.get('/');
  }

  typeSearchedTags(tags: string) {
    this.searchImagesTagInput
      .sendKeys(tags);
  }

  startSearch() {
    this.searchImagesSubmitButton
      .click();
  }

  clickOnImage(imageIndex: number) {
    this.imageComponents
      .get(imageIndex)
      .click();
  }
}
