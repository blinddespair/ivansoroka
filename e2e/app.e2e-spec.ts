import { AppPage } from './app.po';
import {browser} from 'protractor';

describe('jofogas-homeassignment App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should search for images', () => {
    const searchedTags = 'flower';

    page.navigateTo();
    page.typeSearchedTags(searchedTags);
    page.startSearch();

    expect(page.imageComponents.isPresent()).toBeTruthy();
  });

  it('should open image details', () => {
    const searchedTags = 'flower';
    const imageIndex = 2;

    page.navigateTo();
    page.typeSearchedTags(searchedTags);
    page.startSearch();
    page.clickOnImage(imageIndex);

    expect(page.imageDetailsComponent.isPresent()).toBeTruthy();
  });

  it('should show loading wheel when opened image details', () => {
    const searchedTags = 'flower';
    const imageIndex = 2;

    page.navigateTo();
    page.typeSearchedTags(searchedTags);
    page.startSearch();
    page.clickOnImage(imageIndex);
    browser.waitForAngularEnabled(false);

    expect(page.imageDetailsLoadingWheel.isPresent()).toBeTruthy();
    browser.waitForAngularEnabled(true);
  });

  it('should not show loading wheel when opened image details and data was loaded', () => {
    const searchedTags = 'flower';
    const imageIndex = 2;

    page.navigateTo();
    page.typeSearchedTags(searchedTags);
    page.startSearch();
    page.clickOnImage(imageIndex);

    expect(page.imageDetailsLoadingWheel.isPresent()).toBeFalsy();
  });
});
